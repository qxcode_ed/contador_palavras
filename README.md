# Contador de Palavras

A proposta dessa atividade é manipular textos e contar listas palavras distintas e suas respectivas ocorrências. 

### Requisitos
  - Manipulação de arquivos utilizando de preferência o ``` ifstream ``` da bilioteca ``` <iostream> ```;
  - Mapas (chave_unica, valor) ``` map <string, int> mapa; ```
  - ``` iterator ``` sobre um ```<map>```

## Funções
Podemos dividir o trabalho em duas etapas básicas:

* Capturar as palavras do texto;
* Imprimir as palavras e suas respectivas ocorrências;

### Capturar Palavras
Garanta que exista um método que percorra todo o texto e que as palavras sejam adicionadas sem espaços, pontuação e lembre-se, o C++ é case sensitive (Aluno != aluno != ALUNO), então, para esse trabalho, isso deve ser verdadeiro (Aluno == aluno == ALUNO) ;

Cada palavra DISTINTA (```<chave>```) capturada deve ser inserida no ``` <map>``` e possuir repetição  (```<valor>```) igual a 1, caso seja uma palavra já existente no map, a repetição deve ser incrementada.

### Imprimir Palavras
Pesquise por iteradores em c++, é simples, muito similar as iterações com ```while``` e ```for```.

### Verifique 2 arquivos
Use as funções criadas para analisar quantas palavras que formam 80% de ambos os textos coincidem entre si.
Use um vector para cada arquivo, conte as coincidências e verifique a porcentagem.