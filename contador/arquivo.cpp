#include "arquivo.h"

#include <iostream>
#include <string>

using namespace std;

Arquivo::Arquivo(string caminho_arquivo)
{
    this->caminho_arquivo = caminho_arquivo;
    this->quantidade_palavras = 0;
}

string Arquivo::getCaminho(){
    return this->caminho_arquivo;
}

void Arquivo::setCaminho(string caminho_arquivo){
    this->caminho_arquivo = caminho_arquivo;
}

int Arquivo::getQuantidadePalavras()
{
    return this->quantidade_palavras;
}

void Arquivo::addQuantidadePalavras()
{
    this->quantidade_palavras++;
}
