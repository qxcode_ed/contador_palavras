#ifndef ARQUIVO_H
#define ARQUIVO_H

#include <string>

using namespace std;

class Arquivo
{
private:
    string caminho_arquivo;
public:
    int quantidade_palavras;
    Arquivo(string caminho_arquivo);
    std::string getCaminho();
    void setCaminho(string caminho_arquivo);
    int getQuantidadePalavras();
    void addQuantidadePalavras();
};

#endif // ARQUIVO_H
