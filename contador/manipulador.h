#ifndef MANIPULADOR_H
#define MANIPULADOR_H

#include "arquivo.h"

#include <iostream>
#include <map>
#include <fstream>
#include <string>

using namespace std;

class Manipulador
{
private:
    map <string, int> mapas_palavras;
    string buffer_palavras;
    ifstream fluxo_arquivo;
    Arquivo * arquivo;
public:
    Manipulador(Arquivo *arquivo);
    void capturaPalavras();
    void imprimeListaPalavras();
    void imprimeLog();
    ~Manipulador();

};

#endif // MANIPULADOR_H
