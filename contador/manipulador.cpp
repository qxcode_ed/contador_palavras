#include "manipulador.h"
#include "arquivo.h"

#include <iostream>
#include <iomanip>

#include <map>
#include <fstream>
#include <string>

using namespace std;

int qtd = 0;

Manipulador::Manipulador(Arquivo * arquivo)
{
    this->arquivo = arquivo;
    this->fluxo_arquivo.open(arquivo->getCaminho().c_str());
}

void Manipulador::capturaPalavras()
{
    while (this->fluxo_arquivo >> this->buffer_palavras)
    {
        this->arquivo->quantidade_palavras++;

        int indice_pontuacao;
        while ((indice_pontuacao = this->buffer_palavras.find_first_of(".,!?_;\"")) != -1) //string::npos
        {
            this->buffer_palavras.erase(indice_pontuacao, 1);
        }

        ++this->mapas_palavras[this->buffer_palavras];
    }
}

void Manipulador::imprimeListaPalavras()
{
    map<string, int>::const_iterator it(this->mapas_palavras.begin());

    while (it != this->mapas_palavras.end())
    {
        cout << "Palavra [ " << setw(20) << std::left << it->first << " ] "<< setw(10)
             << " Ocorrencias: " << it->second << endl;
        ++it;
    }

}

void Manipulador::imprimeLog()
{
    cout << "Quantidade de Palavras: " << this->arquivo->getQuantidadePalavras()<< endl;
    cout << "Quantidade de Palavras Distintas: " << this->mapas_palavras.size()<< endl;
}

Manipulador::~Manipulador()
{
    this->fluxo_arquivo.close();
    delete(this->arquivo);
}



