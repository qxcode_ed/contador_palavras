#include "arquivo.h"
#include "manipulador.h"

#include <iostream>
#include <string.h>

using namespace std;

int main()
{
    Arquivo * arquivo = new Arquivo("../data/peter_pan_full.txt");

    Manipulador * manipulador = new Manipulador(arquivo);
    manipulador->capturaPalavras();
    manipulador->imprimeListaPalavras();
    manipulador->imprimeLog();
}
